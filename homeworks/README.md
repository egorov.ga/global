# SQL-EX Homework

### Как сдавать задание

 1. Зарегистрироваться на [sql-ex](https://www.sql-ex.ru/)
 2. Выполнять задачи (см. `Организационные моменты`) из раздела `"Упражнения по SQL/SELECT (обучающий этап, выбор СУБД)"`
![](pics/pic-1.png)
 3. После успешного написания запроса и решения поставленной задачи необходимо отправить sql код в файле в 
    соответствующей ветке*. Имя файла: `task-<НОМЕР ЗАДАЧИ НА САЙТЕ>.sql`. Для каждого решения **обязательно** в 
    описании к 
    Merge 
    request приложить 
    скрины с 
    подтверждением пройденных тестов. На скриншоте должны быть видны:
    * номер задачи
    * в списке `"Выбор СУБД"` должен быть `PGSQL`
    * видно "Правильно" после выполнения запрос
    * сам запрос (код должен совпадать с тем, что на Gitlab)
![](pics/pic-2.png)
 4. *Имя ветки определяется порядковым номером задачи в табличке ниже. Например, для 3го дз 5ая задача - это задача 
    23 на sql.ex.

### Организационные моменты

|   № ДЗ | Вес   |   Задачи     | Темы         | Дедлайн  | 
|-------:|:------|-------------:|-------------:|:--------:|
|    1   | `0.2` | 1, 2, 10, 17, 31, 33, 38, 44, 53, 62 | Простые запросы | 28-03-2022 21:00 GMT+03:00 |
|    2   | `0.3` | 11, 12, 15, 20, 22, 29, 30, 68, 74, 86 | Агрегации и группировки | 28-03-2022 21:00 GMT+03:00 |
|    3   | `0.4` | 7, 9, 14, 16, 23, 32, 48, 84 | Соединения| 04-04-2022 21:00 GMT+03:00 |
|    4   | `0.5` | 37, 41, 47, 70, 73, 87, 102, 112 | Сложные запросы | 11-04-2022 21:00 GMT+03:00 |
|    5   | `0.6` | 65, 82, 90, 96, 98, 100, 105, 109 | СTE & WF | 18-04-2022 21:00 GMT+03:00 |

Указанные дедлайны являются **жесткими**.

P.S.: выполнять и отправлять решения на проверку можно с момента публикации.
